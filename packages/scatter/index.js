import Scatter from './src/scatter'

Scatter.install = function (Vue) {
  Vue.component(Scatter.name, Scatter)
}
export default Scatter
